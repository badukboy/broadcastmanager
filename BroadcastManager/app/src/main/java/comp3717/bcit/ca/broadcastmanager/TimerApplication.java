package comp3717.bcit.ca.broadcastmanager;


import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;


public class TimerApplication
    extends Application
{
    private static final String TAG = TimerApplication.class.getName();
    private LocalBroadcastManager broadcastManager;
    private BroadcastMessages.StartTimerReceiver startTimerReceiver;
    private BroadcastMessages.StopTimerReceiver stopTimerReceiver;
    private Timer timer;

    @Override
    public void onCreate() {
        super.onCreate();

        broadcastManager = LocalBroadcastManager.getInstance(this);
        startTimerReceiver = new BroadcastMessages.StartTimerReceiver()
        {
            @Override
            protected void doOnReceive(final Context context,
                                       final Intent intent)
            {
                Log.w(TAG, "startTimerReceiver");

                timer = new Timer("fun");
                timer.schedule(new TimerTask()
                               {
                                   @Override
                                   public void run()
                                   {
                                       BroadcastMessages.postTimerFiredMessage(broadcastManager);
                                   }
                               },
                               0,
                               1000);
            }
        };
        stopTimerReceiver = new BroadcastMessages.StopTimerReceiver()
        {
            @Override
            protected void doOnReceive(final Context context,
                                       final Intent intent)
            {
                Log.w(TAG, "stopTimerReceiver");
                timer.cancel();
            }
        };

        BroadcastMessages.registerStartTimerReceiver(startTimerReceiver,
                                                     broadcastManager);
        BroadcastMessages.registerStopTimerReceiver(stopTimerReceiver,
                                                    broadcastManager);
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(final int level)
    {
        super.onTrimMemory(level);
    }
}
