package comp3717.bcit.ca.broadcastmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by darcy on 2016-09-26.
 */
public class BroadcastMessages
{
    private static final String TAG                 = BroadcastMessages.class.getName();
    public static final String  START_TIMER_MESSAGE = "startTimer";
    public static final String  STOP_TIMER_MESSAGE  = "stopTimer";
    public static final String  TIMER_FIRED_MESSAGE = "timerFired";
    public static final String PERIOD_KEY = "period";

    private BroadcastMessages()
    {
    }

    private static void registerReceiver(final @NonNull String name,
                                         final @NonNull BroadcastReceiver receiver,
                                         final @NonNull LocalBroadcastManager broadcastManager)
    {
        final IntentFilter filter;

        Log.i(TAG,
              "registering " + name);
        filter = new IntentFilter(name);
        broadcastManager.registerReceiver(receiver,
                                          filter);
    }

    private static void post(final @NonNull String messageName,
                             final @NonNull LocalBroadcastManager broadcastManager)
    {
        final Intent intent;

        intent = new Intent();
        post(messageName, intent, broadcastManager);
    }

    private static void post(final @NonNull String messageName,
                             final @NonNull Intent intent,
                             final @NonNull LocalBroadcastManager broadcastManager)
    {
        final Bundle extras;

        extras = intent.getExtras();
        intent.setAction(messageName);

        if(extras == null)
        {
            Log.i(TAG,
                  "broadcasting " + messageName);
        }
        else
        {
            Log.i(TAG,
                  "broadcasting " + messageName + " " + extras.toString());
        }

        broadcastManager.sendBroadcast(intent);
    }
    private static abstract class AbstractReceiver
        extends BroadcastReceiver
    {
        private final String expectedAction;

        protected AbstractReceiver(final String action)
        {
            expectedAction = action;
        }

        @Override
        public final void onReceive(final Context context,
                                    final Intent intent)
        {
            final String action;

            action = intent.getAction();

            if(action.equals(expectedAction))
            {
                doOnReceive(context,
                            intent);
            }
            else
            {
                Log.w(TAG,
                      action +
                      " does not equal " + expectedAction);
            }
        }

        protected abstract void doOnReceive(Context context,
                                            Intent intent);
    }

    public abstract static class StartTimerReceiver
        extends AbstractReceiver
    {
        public StartTimerReceiver()
        {
            super(START_TIMER_MESSAGE);
        }
    }

    public abstract static class StopTimerReceiver
        extends AbstractReceiver
    {
        public StopTimerReceiver()
        {
            super(STOP_TIMER_MESSAGE);
        }
    }

    public abstract static class TimerFiredReceiver
        extends AbstractReceiver
    {
        public TimerFiredReceiver()
        {
            super(TIMER_FIRED_MESSAGE);
        }
    }

    public static void removeObserver(final @Nullable BroadcastReceiver receiver,
                                      final @NonNull LocalBroadcastManager broadcastManager)
    {
        if (receiver == null)
        {
            Log.w(TAG,
                  "typing to remove a null receiver");
        }
        else
        {
            Log.i(TAG,
                  "removing " + receiver.getClass().getName());
            broadcastManager.unregisterReceiver(receiver);
        }
    }

    public static void registerStartTimerReceiver(final @NonNull StartTimerReceiver receiver,
                                                  final @NonNull LocalBroadcastManager broadcastManager)
    {
        registerReceiver(START_TIMER_MESSAGE,
                         receiver,
                         broadcastManager);
    }

    public static void registerStopTimerReceiver(final @NonNull StopTimerReceiver receiver,
                                                 final @NonNull LocalBroadcastManager broadcastManager)
    {
        registerReceiver(STOP_TIMER_MESSAGE,
                         receiver,
                         broadcastManager);
    }

    public static void registerTimerFiredReceiver(final @NonNull TimerFiredReceiver receiver,
                                                  final @NonNull LocalBroadcastManager broadcastManager)
    {
        registerReceiver(TIMER_FIRED_MESSAGE,
                         receiver,
                         broadcastManager);
    }

    public static void postStartTimerMessage(final @NonNull LocalBroadcastManager broadcastManager,
                                             final long period)
    {
        final Intent intent;

        intent = new Intent();
        intent.putExtra(PERIOD_KEY,
                        period);
        post(START_TIMER_MESSAGE,
             intent,
             broadcastManager);
    }

    public static void postStopTimerMessage(final @NonNull LocalBroadcastManager broadcastManager)
    {
        post(STOP_TIMER_MESSAGE,
             broadcastManager);
    }

    public static void postTimerFiredMessage(final @NonNull LocalBroadcastManager broadcastManager)
    {
        post(TIMER_FIRED_MESSAGE,
             broadcastManager);
    }

    public static int getPeriod(final @NonNull Intent intent,
                                final int defaultValue)
    {
        final int period;

        period = intent.getIntExtra(PERIOD_KEY,
                                    defaultValue);

        return (period);
    }
}
