package comp3717.bcit.ca.broadcastmanager;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity
    extends AppCompatActivity
{
    private static final String TAG = MainActivity.class.getName();
    private LocalBroadcastManager broadcastManager;
    private BroadcastMessages.TimerFiredReceiver timerFiredReceiver;
    private int counter;

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        broadcastManager = LocalBroadcastManager.getInstance(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timerFiredReceiver = new BroadcastMessages.TimerFiredReceiver()
        {
            @Override
            protected void doOnReceive(final Context context,
                                       final Intent intent)
            {
                Log.w(TAG,
                      "timerFiredReceiver");
                counter++;
                Log.w(TAG, "" + counter);

                if(counter > 4)
                {
                    BroadcastMessages.postStopTimerMessage(broadcastManager);
                }
            }
        };

        BroadcastMessages.registerTimerFiredReceiver(timerFiredReceiver,
                                                     broadcastManager);
        BroadcastMessages.postStartTimerMessage(broadcastManager, 1000);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        BroadcastMessages.removeObserver(timerFiredReceiver, broadcastManager);
    }
}
